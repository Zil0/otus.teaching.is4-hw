﻿using IdentityModel.Client;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Otus.IS4HW.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var client = new HttpClient();

            var disco = await client.GetDiscoveryDocumentAsync("https://localhost:5001");
            if (disco.IsError)
            {
                throw new ArgumentException(nameof(disco));
            }

            var response = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest()
            {
                Address = disco.TokenEndpoint,
                ClientId = "m2m.client",
                ClientSecret = "511536EF-F270-4058-80CA-1C89C192F69A",
                Scope = "scope1"
            });

            if (response.IsError)
            {
                throw new ArgumentException(nameof(response));
            }

            Console.WriteLine(response.Json);

            var api = new HttpClient();
            api.SetBearerToken(response.AccessToken);

            var result = await api.GetAsync("https://localhost:7001/Hello/Get");
            if (!result.IsSuccessStatusCode)
            {
                Console.WriteLine("Bad result");
                Console.WriteLine(result.StatusCode);
            }

            Console.WriteLine(await result.Content.ReadAsStringAsync());

            Console.ReadKey();
        }
    }
}
